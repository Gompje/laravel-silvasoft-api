<?php
namespace DaktaDeo\Silvasoft;
use hamburgscleanest\GuzzleAdvancedThrottle\RequestLimitRuleset;
use Illuminate\Cache\Repository;

trait NeedsThrottling {
	/**
	 * @param int $api_request_limit
	 *
	 * @return RequestLimitRuleset
	 */
	public function getRequestLimitRuleset($api_request_limit=15){
		return new RequestLimitRuleset([
			'https://mijn.silvasoft.nl' => [
				[
					'max_requests'     => $api_request_limit,
					'request_interval' => 3600
				]
			]
		],
			'cache' // caching strategy
		);
	}
}
