<?php

namespace DaktaDeo\Silvasoft;
use DaktaDeo\Silvasoft\Exceptions\BadRequest;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

trait HasProducts {
	/**
	 * From the Silvasoft docs:
	 *
	 * Retrieve a list of products from your administration. A maximum of 100 products will be returned at once.
	 * Use offset and limit parameters to retrieve more products over multiple calls.
	 * It is also possible to retrieve just one specific product by adding the ArticleNumber or EAN request parameter.
	 *
	 * @param array $parameters
	 *
	 * @return Collection of Products
	 * @throws Exception
	 */
	public function listProducts( Array $parameters ) {
		$arr = $this->getEndpointRequest( 'listproducts/', $parameters );
		$lst = Product::hydrate( $arr );
		
		return $lst->flatten();
	}
	
	/**
	 * From the Silvasoft docs:
	 *
	 * This POST method allows you to create a new product (catalog article).
	 *
	 * Notes:
	 *
	 * ArticleNumber, NewName and CategoryName are mandatory fields
	 * You can set CategoryCreateIfMissing to true if you want the category to be created if it is not found.
	 * The request and response parameters are the same as the request parameters for the endpoint ‘UpdateProduct’.
	 * Please see the endpoint updateproduct for more details.
	 *
	 * @param Product $product
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function addProduct( Product $product ) {
		if ( blank( $product->ArticleNumber ) ) {
			throw new IsRequired( "ArticleNumber" );
		}
		if ( blank( $product->NewName ) ) {
			throw new IsRequired( "NewName" );
		}
		if ( blank( $product->CategoryName ) ) {
			throw new IsRequired( "CategoryName" );
		}
		$options = $product->toArray();
		
		return $this->postEndpointRequest( 'addproduct/', $options );
	}
	
	
	/**
	 * From the Silvasoft docs:
	 *
	 * Update some product fields for a specific product. For example update product stock quantity or product price.
	 *
	 * Note that it is not required to set all request parameters. For example, if you just want to update
	 * the stock value, there is no need to set the NewName parameter as well.
	 *
	 * Updating the stock quantity for a grouped product ('verzamelproduct')?
	 * Please read the following notes when updating the stock quantity for grouped products:
	 * Updating the stock of a grouped product can only be a relative update. The parameter ‘StockUpdateMode’ is ignored because of this.
	 * The returned stock quantity will be the newly calculated stock quantity for the grouped product.
	 * Updating a grouped product will update ALL child products with the given stock quantity based on the amount set in the grouped product.
	 * An example: your grouped product consists of 10 times product X and 4 times product Y. Now let’s say
	 * you want to update the grouped product with say a ‘NewStockQty’ of 2. This means you have 2 extra units
	 * available for your grouped product. Meaning this will increase the stock quantity for product X with 20 and product Y with 8.
	 *
	 * @param Product $product
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function updateProduct(Product $product){
		if ( blank( $product->ArticleNumber ) && blank( $product->EAN )  ) {
			throw new IsRequired( "ArticleNumber or EAN " );
		}		
		$options = $product->toArray();
		
		return $this->putEndpointRequest( 'updateproduct/', $options );
	}
}