<?php
namespace DaktaDeo\Silvasoft\Test;

use DaktaDeo\Silvasoft\Client;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use DaktaDeo\Silvasoft\Relation;
use GuzzleHttp\Client as GuzzleClient;
//use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Silvasoft;

class CrmTest extends TestCase {
	use HasMockGuzzleRequests;
	
	protected $test_key = "b7d343Bnhd436f3ec3bd3504582";
	protected $test_username = "john@doe.nl";
	
	/** @test */
	public function it_can_get_all_relations() {
		$response   = array(
			0 =>
				array(
					'IsCustomer'              => true,
					'Email'                   => 'info@johnrealestate.com',
					'PrivateOrBusiness'       => 'BusinessRelation',
					'Address_City'            => 'New-York',
					'SupplierNumber'          => null,
					'Address_PostalCode'      => '25252',
					'Address_Street'          => '1th avenue',
					'Name'                    => 'John Real Estate',
					'Address_CountryCode'     => 'US',
					'Relation_Contact'        =>
						array(
							0 =>
								array(
									'Email'          => 'john@doe.nl',
									'ContactNotes'   => 'Example note',
									'FamilyName'     => 'Doe',
									'FirstName'      => 'John',
									'Phone'          => null,
									'Sex'            => 'Man',
									'LastName'       => 'Doe',
									'Preposition'    => null,
									'DefaultContact' => true,
								),
							1 =>
								array(
									'Email'          => 'james@doe.nl',
									'ContactNotes'   => 'Johns brother',
									'FamilyName'     => 'Doe',
									'FirstName'      => 'James',
									'Phone'          => null,
									'Sex'            => 'Man',
									'LastName'       => 'Doe',
									'Preposition'    => 'from',
									'DefaultContact' => true,
								),
						),
					'Address_Unit'            => '1a',
					'IsSupplier'              => false,
					'CustomerNumber'          => 1,
					'IsOther'                 => false,
					'Phone'                   => null,
					'RegistrationNumber'      => null,
					'TaxIdentificationNumber' => null,
					'RelationGUID'            => 8765432,
				),
			1 =>
				array(
					'IsCustomer' => true,
					'Email'      => 'info@anothercustomer',
				),
		);
		$endpoint   = 'https://mijn.silvasoft.nl/rest/listrelations/';
		$parameters = [ "limit" => 100, "relationtype" => "All" ];
		
		$mockGuzzle = $this->mock_get_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$items      = $client->listRelations( $parameters );
		$this->assertCount( 2, $items );
	}
	
	/** @test */
	public function when_posting_a_new_relation_name_is_required(){
		$this->expectException(IsRequired::class);
		
		$relation = new Relation();
		$relation->email = "john@doe.nl";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "post" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->addBusinessRelation($relation);
	}
	
	/** @test */
	public function it_can_add_a_new_bussiness_relation(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/addbusinessrelation/';
		$response   = array(
			0 =>
				array(
					'IsCustomer'              => true,
					'Email'                   => 'info@johnrealestate.com',
					'PrivateOrBusiness'       => 'BusinessRelation',
					'Address_City'            => 'New-York',
					'SupplierNumber'          => null,
					'Address_PostalCode'      => '25252',
					'Address_Street'          => '1th avenue',
					'Name'                    => 'John Real Estate',
					'Address_CountryCode'     => 'US',
					'Relation_Contact'        =>
						array(
							0 =>
								array(
									'Email'          => 'john@doe.nl',
									'ContactNotes'   => 'Example note',
									'FamilyName'     => 'Doe',
									'FirstName'      => 'John',
									'Phone'          => null,
									'Sex'            => 'Man',
									'LastName'       => 'Doe',
									'Preposition'    => null,
									'DefaultContact' => true,
								),
							1 =>
								array(
									'Email'          => 'james@doe.nl',
									'ContactNotes'   => 'Johns brother',
									'FamilyName'     => 'Doe',
									'FirstName'      => 'James',
									'Phone'          => null,
									'Sex'            => 'Man',
									'LastName'       => 'Doe',
									'Preposition'    => 'from',
									'DefaultContact' => true,
								),
						),
					'Address_Unit'            => '1a',
					'IsSupplier'              => false,
					'CustomerNumber'          => 1,
					'IsOther'                 => false,
					'Phone'                   => null,
					'RegistrationNumber'      => null,
					'TaxIdentificationNumber' => null,
					'RelationGUID'            => 8765432,
				)
		);
		
		$data = Relation::hydrate($response);
		$data->flatten();
		$relation = $data->first();
		$parameters = ["json" => $relation->toArray()];
		
		$mockGuzzle = $this->mock_post_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->addBusinessRelation($relation);
		
		$this->assertCount( 1, $items );
		$this->assertSame($relation->toArray(), $items[0]);
	}
	
	/** @test */
	public function when_posting_a_new_privaterelation_fields_are_required(){
		$this->expectException(IsRequired::class);
		
		$relation = new Relation();
		$relation->email = "john@doe.nl";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "post" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->addPrivateRelation($relation);
	}
	
	/** @test */
	public function it_can_add_a_new_private_relation(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/addprivaterelation/';
		$response   = [array (
			'Address_City' => 'Amsterdam',
			'Address_Street' => 'Streetname',
			'Address_Unit' => '4',
			'IsCustomer' => true,
			'IsOther' => true,
			'Relation_Contact' =>
				array (
					'Email' => 'john@doe.com',
					'Phone' => '123-4565-98',
					'FirstName' => 'John',
					'LastName' => 'Doe',
					'Sex' => 'Man',
					'ContactNotes' => 'Note: consumer John is assigned special discount by Hank',
				)
		)];
		
		$data = Relation::hydrate($response);
		$data->flatten();
		$relation = $data->first();
		
		$parameters = ["json" => $relation->toArray()];
		
		$mockGuzzle = $this->mock_post_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->addPrivateRelation($relation);
		
		$this->assertCount( 1, $items );
		$this->assertSame($relation->toArray(), $items[0]);
	}
	
	/** @test */
	public function when_posting_an_update_relation_fields_are_required(){
		$this->expectException(IsRequired::class);
		
		$relation = new Relation();
		$relation->email = "john@doe.nl";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "put" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->updateRelation($relation);
	}
	
	/** @test */
	public function it_can_update_a_relation(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/updaterelation/';
		$response   = [array (
			'RelationGUID' => 1120426,
			'Email' => 'customerx@example.com',
			'Phone' => '+310123123',
			'Name' => 'New relation name',
			'RegistrationNumber' => '91919191',
			'TaxIdentificationNumber' => '12312312B01',
			'SupplierNumber' => 123123,
			'Address_City' => 'Amsterdam',
			'Address_Street' => 'Streetname',
			'Address_PostalCode' => '1111CC',
			'Address_Unit' => '4',
			'Address_UnitAppendix' => 'Suite B',
			'Address_CountryCode' => 'NL',
			'Relation_Contact' =>
				array (
					0 =>
						array (
							'Email' => 'john@doe.com',
							'Phone' => '123-4565-98',
							'FirstName' => 'John',
							'LastName' => 'Doe',
							'Sex' => 'Man',
							'ContactNotes' => 'Contact notes',
						),
				),
		)];
		
		$data = Relation::hydrate($response);
		$data->flatten();
		$relation = $data->first();
		
		$parameters = ["json" => $relation->toArray()];
		
		$mockGuzzle = $this->mock_put_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->updateRelation($relation);
		
		$this->assertCount( 1, $items );
		$this->assertSame($relation->toArray(), $items[0]);
	}
	
	
}