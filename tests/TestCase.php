<?php

namespace DaktaDeo\Silvasoft\Test;

use DaktaDeo\Silvasoft\Facade;
use DaktaDeo\Silvasoft\ServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase {
	/**
	 * Load package service provider
	 *
	 * @param  \Illuminate\Foundation\Application $app
	 *
	 * @return \DaktaDeo\Silvasoft\ServiceProvider
	 */
	protected function getPackageProviders( $app ) {
		return [ ServiceProvider::class ];
	}
	
	/**
	 * Load package alias
	 *
	 * @param  \DaktaDeo\Silvasoft\Facade $app
	 *
	 * @return array
	 */
	protected function getPackageAliases( $app ) {
		return [
			'Silvasoft' => Facade::class,
		];
	}
}