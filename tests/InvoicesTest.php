<?php
namespace DaktaDeo\Silvasoft\Test;

use DaktaDeo\Silvasoft\Client;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use DaktaDeo\Silvasoft\Invoice;
use GuzzleHttp\Client as GuzzleClient;
//use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Silvasoft;

class InvoicesTest extends TestCase {
	use HasMockGuzzleRequests;
	
	protected $test_key = "b7d343Bnhd436f3ec3bd3504582";
	protected $test_username = "john@doe.nl";
	
	/** @test */
	public function it_can_get_all_invoices() {
		$response   = array (
			0 =>
				array (
					'InvoiceReference' => '',
					'InvoiceOutstandingAmount' => 42.35000000000000142108547152020037174224853515625,
					'InvoiceNotes' => 'BOOKKEEPING INVOICE EXAMPLE',
					'ProjectName' => 'Example project',
					'ProjectNumber' => '1',
					'InvoiceTotalInclTax' => 42.35000000000000142108547152020037174224853515625,
					'InvoiceType' => 'BOOKKEEPING',
					'PackingSlipNotes' => '',
					'CustomerNumber' => 1,
					'InvoiceTaxAmount' => 7.3499999999999996447286321199499070644378662109375,
					'InvoiceDueDate' => '',
					'CustomerName' => 'Demo company',
					'InvoiceNumber' => 2,
					'InvoiceDate' => '03-08-2017',
					'InvoiceTotalExclTax' => 35,
					'Invoice_Contact' =>
						array (
							0 =>
								array (
									'ContactType' => 'Invoice',
									'Email' => 'example@gmail.com',
									'ContactNotes' => NULL,
									'FamilyName' => 'Janssens',
									'FirstName' => 'Hendrik',
									'Phone' => NULL,
									'Sex' => 'Man',
									'LastName' => 'Janssens',
									'DefaultContact' => true,
								),
							1 =>
								array (
									'ContactType' => 'PackingSlip',
									'Email' => 'example@gmail.com',
									'ContactNotes' => NULL,
									'FamilyName' => 'Janssens',
									'FirstName' => 'Hendrik',
									'Phone' => NULL,
									'Sex' => 'Man',
									'LastName' => 'Janssens',
									'DefaultContact' => true,
								),
						),
					'Invoice_Address' =>
						array (
							0 =>
								array (
									'Address_CountryCode' => 'BE',
									'Address_Unit' => '55',
									'Address_City' => 'Antwerpen',
									'Address_UnitAppendix' => '',
									'AddressType' => 'InvoiceAddress',
									'Address_PostalCode' => '2018',
									'Address_Street' => 'Voorbeeldstraat',
								),
						),
				),
			1 =>
				array (
					'InvoiceReference' => 'Invoice reference example',
					'InvoiceOutstandingAmount' => 4774.7200000000002546585164964199066162109375,
					'InvoiceNotes' => '
INVOICE EXAMPLE 1
n',
					'ProjectName' => '',
					'ProjectNumber' => '',
					'InvoiceTotalInclTax' => 4774.7200000000002546585164964199066162109375,
					'InvoiceType' => 'INVOICING',
					'PackingSlipNotes' => '',
					'CustomerNumber' => 1,
					'InvoiceTaxAmount' => 828.6699999999999590727384202182292938232421875,
					'InvoiceDueDate' => '',
					'CustomerName' => 'Demo company',
					'InvoiceNumber' => 1,
					'InvoiceDate' => '03-08-2017',
					'InvoiceTotalExclTax' => 3946.0500000000001818989403545856475830078125,
					'Invoice_Contact' =>
						array (
							0 =>
								array (
									'ContactType' => 'Invoice',
									'Email' => 'example@gmail.com',
									'ContactNotes' => NULL,
									'FamilyName' => 'Janssens',
									'FirstName' => 'Hendrik',
									'Phone' => NULL,
									'Sex' => 'Man',
									'LastName' => 'Janssens',
									'DefaultContact' => true,
								),
							1 =>
								array (
									'ContactType' => 'PackingSlip',
									'Email' => 'example@gmail.com',
									'ContactNotes' => NULL,
									'FamilyName' => 'Janssens',
									'FirstName' => 'Hendrik',
									'Phone' => NULL,
									'Sex' => 'Man',
									'LastName' => 'Janssens',
									'DefaultContact' => true,
								),
						),
					'Invoice_Address' =>
						array (
							0 =>
								array (
									'Address_CountryCode' => 'BE',
									'Address_Unit' => '55',
									'Address_City' => 'Antwerpen',
									'Address_UnitAppendix' => '',
									'AddressType' => 'InvoiceAddress',
									'Address_PostalCode' => '2018',
									'Address_Street' => 'Voorbeeldstraat',
								),
						),
				),
		);
		$endpoint   = 'https://mijn.silvasoft.nl/rest/listsalesinvoices/';
		$parameters = [ "limit" => 100];
		
		$mockGuzzle = $this->mock_get_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$items      = $client->listSalesInvoices( $parameters );
		$this->assertCount( 2, $items );
	}
	

	
	/** @test */
	public function it_can_add_a_new_sales_transaction(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/addsalestransaction/';
		$response   = [array (
			'CustomerNumber' => 1,
			'InvoiceNotes' => 'Invoices created from API',
			'InvoiceDate' => '05-05-2017',
			'PackingSlipNotes' => 'Extra note to be added to the packing-slip',
			'InvoiceProject' => 'Pr-123',
			'SelectProjectBy' => 'NAME',
			'Invoice_InvoiceLine' =>
				array (
					0 =>
						array (
							'TaxPC' => 6,
							'SubTotalInclTax' => 400,
							'Description' => 'Fixed price product line, product selected by SKU',
						),
					1 =>
						array (
							'TaxPC' => 21,
							'SubTotalInclTax' => 400,
							'Description' => 'Fixed price product line, product selected by EAN',
						),
				),
		)];
		
		$data = Invoice::hydrate($response);
		$data->flatten();
		$invoice = $data->first();
		$parameters = ["json" => $invoice->toArray()];
		
		$mockGuzzle = $this->mock_post_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->addSalesTransaction($invoice);
		
		$this->assertCount( 1, $items );
		$this->assertSame($invoice->toArray(), $items[0]);
	}
	
	/** @test */
	public function it_can_add_a_new_invoice(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/addsalesinvoice/';
		$response   = [array (
			'CustomerNumber' => 1,
			'InvoiceNotes' => 'Invoices created from API',
			'InvoiceReference' => 'system-X-1234',
			'InvoiceDate' => '05-05-2017',
			'InvoiceDiscountPc' => 15,
			'InvoiceDueDays' => 21,
			'PackingSlipNotes' => 'Extra note to be added to the packing-slip',
			'InvoiceProject' => 'Pr-123',
			'SelectProjectBy' => 'NAME',
			'Invoice_InvoiceLine' =>
				array (
					0 =>
						array (
							'ProductNumber' => '1',
							'Quantity' => '36',
							'TaxPC' => 21,
							'UnitPriceExclTax' => 3.95000000000000017763568394002504646778106689453125,
							'DiscountPc' => 25,
							'Description' => 'Product line with price per unit and 25% discount',
						),
					1 =>
						array (
							'ProductNumber' => '2',
							'Quantity' => '25',
							'TaxPC' => 6,
							'SubTotalInclTax' => 400,
							'Description' => 'Fixed price product line, product selected by SKU',
						),
					2 =>
						array (
							'ProductEAN' => '123456789',
							'Quantity' => '25',
							'TaxPC' => 21,
							'SubTotalInclTax' => 400,
							'Description' => 'Fixed price product line, product selected by EAN',
						),
				),
		)];
		
		$data = Invoice::hydrate($response);
		$data->flatten();
		$invoice = $data->first();
		$parameters = ["json" => $invoice->toArray()];
		
		$mockGuzzle = $this->mock_post_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->addSalesInvoice($invoice);
		
		$this->assertCount( 1, $items );
		$this->assertSame($invoice->toArray(), $items[0]);
	}
	
	
	/** @test */
	public function when_posting_a_new_transaction_fields_are_required(){
		$this->expectException(IsRequired::class);
		
		$invoice = new Invoice();
		$invoice->PackingSlipNotes = "Extra note to be added to the packing-slip";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "post" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->addSalesTransaction($invoice);
	}
	
	/** @test */
	public function when_posting_a_new_invoice_fields_are_required(){
		$this->expectException(IsRequired::class);
		
		$invoice = new Invoice();
		$invoice->PackingSlipNotes = "Extra note to be added to the packing-slip";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "post" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->addSalesInvoice($invoice);
	}
}